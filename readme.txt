README
===============================================================================================================
Author: VINAY KANT SAHU
Email: vinaykant.sahu@gmail.com
Created Date: Sep 10, 2015
===============================================================================================================
This is very basic MVC framework written from scratch, to run this system please follow below steps
1. Thank your for download. :)
2. Keep this payroll folder under the webroot folder 
wamp: c://wamp/www/payroll
lamp: /var/html/www/payroll
PS: In case of name change of code directory, please make proper changes in .htaccess file for the RewriteBase setting
 
3. Make sure mod_rewrite is enabled on Apache server
4. Set 0777 permission to the folder name "output"
5. Open config/settings.php file and make proper changes if any thing changed
6. To run through URL use http://localhost/payroll
7. To run through command line
use below commands
(LAMP/WAMP/MAC)
cd /var/html/www/payroll (In case of change in your file structure please use your path) 
php clNew.php