<?php
require_once '../../library/Base/Loader.php'; // this is the file which can load any class of the project
class Base_ControllersTest extends PHPUnit_Framework_TestCase
{
  public function setUp(){ }
  public function tearDown(){ }

  public function testConstructor()
  {
    $_POST["FileName"]= 'xyz';
    $connObj = new Base_Controllers($this);

	$this->assertTrue($connObj->getParam("FileName", "post") == $_POST["FileName"]);
    
  }
}
?>