<?php
require_once '../../library/Base/Loader.php'; // this is the file which can load any class of the project
class Base_LoaderTest extends PHPUnit_Framework_TestCase
{
  public function setUp(){ }
  public function tearDown(){ }

  public function testConstructor()
  {
    $connObj = new Base_Loader();
	$this->assertTrue($connObj->controller == 'index');
    $this->assertTrue($connObj->actionName == 'index');
	$this->assertTrue(is_array($connObj->parameters) == true);
  }
}
?>