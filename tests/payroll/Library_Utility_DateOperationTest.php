<?php
require_once '../../library/Base/Loader.php'; // this is the file which can load any class of the project
class Library_Utility_DateOperationTest extends PHPUnit_Framework_TestCase
{
  public function setUp(){ }
  public function tearDown(){ }
  //todayDate
  public function testtodayDate()
  {
    $expectedVal = array(date("d"),date("D"),date("m"),date("M"),date("Y")	);
    $connObj = new Library_Utility_DateOperation($this);
	
	$this->assertTrue( $connObj->todayDate()=== $expectedVal);
    
  }
  //nextMonth
  public function testnextMonth()
  {
    $expectedVal = array("08","Aug","2015");
	$expectedVal1 = array("01","Jan","2016");
    $connObj = new Library_Utility_DateOperation($this);
	
	$this->assertTrue( $connObj->nextMonth(7, 2015)=== $expectedVal);
    $this->assertTrue( $connObj->nextMonth(12, 2015)=== $expectedVal);
	
  }
  //lastDayOfMonth
  public function testlastDayOfMonth()
  {
    $expectedVal = array("28","Sat","5");
	$expectedVal1 = array("30","Wed","3");
	$expectedVal2 = array("31","Thu","4");
	$expectedVal3 = array("29","Mon","1");
    $connObj = new Library_Utility_DateOperation($this);
	
	$this->assertTrue( $connObj->lastDayOfMonth(2, 2015)=== $expectedVal);
	$this->assertTrue( $connObj->lastDayOfMonth(9, 2015)=== $expectedVal1);
	$this->assertTrue( $connObj->lastDayOfMonth(12, 2015)=== $expectedVa2);
    $this->assertTrue( $connObj->lastDayOfMonth(2, 2016)=== $expectedVal3);
    
  }
  //dayForDate
  public function testdayForDate()
  {
    $expectedVal = array('Fri','4','2015','07','31'	);
    $expectedVal1 = array('Sat','5','2015','08','15');
    $expectedVal2 = array('Mon','1','2016','02','29');
    $connObj = new Library_Utility_DateOperation($this);
	
	$this->assertTrue( $connObj->dayForDate(31,7,2015)=== $expectedVal);
	$this->assertTrue( $connObj->dayForDate(15,8,2015)=== $expectedVal1);
	$this->assertTrue( $connObj->dayForDate(29,2,2016)=== $expectedVal2);
    
  }
   //nextDateForDayAfterGivenDate
  public function testnextDateForDayAfterGivenDate()
  {
    $expectedVal = array('Fri','4','2015','07','31'	);
    $expectedVal1 = array('Sat','5','2015','08','15');
    $expectedVal2 = array('Mon','1','2016','01','04');
	$expectedVal3 = array('Tue','2','2016','03','01');
    $connObj = new Library_Utility_DateOperation($this);
	
	$this->assertTrue( $connObj->testnextDateForDayAfterGivenDate(30,7,2015,4)=== $expectedVal);
	$this->assertTrue( $connObj->testnextDateForDayAfterGivenDate(8,8,2015,5)=== $expectedVal1);
	$this->assertTrue( $connObj->testnextDateForDayAfterGivenDate(29,12,2015,1)=== $expectedVal2);
	$this->assertTrue( $connObj->testnextDateForDayAfterGivenDate(29,2,2016,2)=== $expectedVal3);
    
  }
}
?>