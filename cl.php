<?php
// Define path to application root directory
define('ROOT_PATH', realpath(dirname(__FILE__)));

require_once ROOT_PATH .'/config/settings.php';
echo "Enter The File Name: \n";
$fileName = trim(fgets(STDIN));

$url= BaseURL."/index/process";
$varParam = "FileName=".$fileName;

$ch = curl_init();
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");	    
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $varParam);
curl_setopt($ch, CURLOPT_URL, $url);    
echo $result = curl_exec($ch); 
