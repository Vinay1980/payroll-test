<?php
/*
 * Class Name: none
 * Author: VINAY KANT SAHU
 * Purpose: This is the root file for webservices, all the request go though this gateway
 * Created Date: Jul 28, 2015
 * */
// Define path to application root directory
define('ROOT_PATH', realpath(dirname(__FILE__)));
// Define direcotory separator for OS compability
define('DS', '/');

// Ensure library is on include_path
set_include_path( implode(PATH_SEPARATOR, array(realpath(ROOT_PATH), realpath(ROOT_PATH .DS. 'library'), get_include_path())));

require_once 'Base/Loader.php';
$loader = new Base_Loader();// call to basic operations
