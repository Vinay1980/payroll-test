<?php
/*
 * Class Name: ErrorController
 * Author: VINAY KANT SAHU
 * Purpose: To handle errors
 * Created Date: Sep 08, 2015
 * */
class ErrorController extends Base_Controllers
{
	public function firstRun()
    {
		//dummy
	}
	/*
	* Function Name: indexMethod
	* Purpose: Deafult action for the controller
	* In Param: none
	* Out Param: None
	* */
	public function indexMethod(){
		echo "The Url is invalid.";
	}
	
}