<?php
/*
 * Class Name: ErrorController
 * Author: VINAY KANT SAHU
 * Purpose: This is the default controller
 * Created Date: Sep 08, 2015
 * */
class indexController extends Base_Controllers
{
	/*
	* Function Name: firstRun
	* Purpose: to perform initial operations
	* In Param: none
	* Out Param: None
	* */
	public function firstRun()
    {
		//default function to run before actual action
	}
	/*
	* Function Name: indexMethod
	* Purpose: Deafult action for the controller
	* In Param: none
	* Out Param: None
	* */
	public function indexMethod(){
		$this->setViewVal("URL", BaseURL);
	}
	/*
	* Function Name: processMethod
	* Purpose: To process payroll and save file with given name
	* In Param: none
	* Out Param: None
	* */
	public function processMethod(){
		$fileName= $this->getParam("FileName","post"); // User Input
		$mode= $this->getParam("Mode","post"); // User Input		
		if($fileName){
			
			$finalRs = array();
			$obj = Library_Utility_DateOperation::todayDate();
			$currMonth = $obj[2];
			$currYear = $obj[4];
			for($i =0; $i < TotalMonths; $i++){
					//get next month info based on current month
					$nextMonth = Library_Utility_DateOperation::nextMonth($currMonth,$currYear);
					$currMonth = $nextMonth[0];
					$currMonthName = $nextMonth[1];
					$currYear = $nextMonth[2];
					
					// get last day info for the month
					$lastDayInfo = Library_Utility_DateOperation::lastDayOfMonth($currMonth,$currYear);
					if(in_array($lastDayInfo[2],array(6,0))){// check weekend for last date
						$diff = ($lastDayInfo[2])?1:2 ;//friday is 5
						$format="d-D-w";
						$timestamp=mktime(0,0,0,$currMonth,$lastDayInfo[0]-$diff, $currYear);
						$lastDayInfo = explode("-",date($format,$timestamp));
					}
					// get day for bonus day 15th of this month
					$bonusDateInfo = Library_Utility_DateOperation::dayForDate(15,$currMonth,$currYear);
					if(in_array($bonusDateInfo[1],array(6,0))){// check weekend for 15th 
						$bonusDateInfo = Library_Utility_DateOperation::nextDateForDayAfterGivenDate(15,$currMonth,$currYear,3); // 3 for wednesday
					}
					$finalRs[] = array("Month"=>$currMonthName, "Year"=>$currYear, "Salary Date"=>$lastDayInfo[0].", ".$lastDayInfo[1], "Bonus Date"=>$bonusDateInfo[4].", ".$bonusDateInfo[0]);
				}
			$filepath = ROOT_PATH .DS.DefaultFilePath.DS.$fileName.".csv";
			Library_Utility_SaveCSV::saveFile($filepath, $finalRs);
			$this->setViewVal("OutputFolder", DefaultFilePath);
			if(!$mode){
				$pathToDwonload = BaseURL .DS.DefaultFilePath.DS.$fileName.".csv";
			}else{
				$pathToDwonload = ROOT_PATH.DS.DefaultFilePath.DS.$fileName.".csv";
			}
			$this->setViewVal("DownloadPath", $pathToDwonload );
		}else{
			$this->redirectTo("index","index");
		}
	}
}