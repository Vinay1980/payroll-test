<?php
/*
 * Class Name: Library_Utility_DateOperation
 * Author: VINAY KANT SAHU
 * Purpose: Utility to perform date operations
 * Created Date: Sep 09, 2015
 * */
final class Library_Utility_DateOperation{
	/*
	* Function Name: todayDate
	* Purpose: To get current date info
	* In Param: none
	* Out Param: array ("d-D-m-M-Y")
	* */
	public static function todayDate(){
		return array(date("d"),date("D"),date("m"),date("M"),date("Y")	);
	}
	/*
	* Function Name: nextMonth
	* Purpose: To get next month info for given month and year
	* In Param: $month, $year
	* Out Param: $result array ("m-M-Y")
	* */
	public static function nextMonth($currMonth, $year){
		$format="m-M-Y";
		$timestamp=mktime(0,0,0,$currMonth+1,1,$year);
		$result = explode("-",date($format,$timestamp));
		return $result;
	}
	/*
	* Function Name: lastDayOfMonth
	* Purpose: To get last date info for given month and year
	* In Param: $month, $year
	* Out Param: $result array ("d-D-w")
	* */
	public static function lastDayOfMonth($month, $year){
		$day=new DateTime("$year-$month-01"); 
		$lastDate = $day->format('t');
		$format="d-D-w";
		$timestamp=mktime(0,0,0,$month,$lastDate ,$year);
		$result = explode("-",date($format,$timestamp));
		return $result;
	}
	/*
	* Function Name: dayForDate
	* Purpose: To get day info for given Date
	* In Param: $date, $month, $year
	* Out Param: $result array ("D-w-Y-m-d")
	* */
	public static function dayForDate($date, $month, $year){
		$format="D-w-Y-m-d";
		$timestamp=mktime(0,0,0,$month,$date ,$year);
		$result = explode("-",date($format,$timestamp));
		return $result;
	}
	/*
	* Function Name: nextDateForDayAfterGivenDate
	* Purpose: To get next Date for a given particular day after given Date
	* In Param: $date, $month, $year, $day
	* Out Param: $result array ("D-w-Y-m-d")
	* */
	public static function nextDateForDayAfterGivenDate($date, $month, $year, $day){ //$day 0-6
		$format="w";
		$timestamp=mktime(0,0,0,$month,$date ,$year);
		$dayForDate= date($format,$timestamp);
		if($dayForDate-$day <0){
			$diff = abs($dayForDate-$day);	
		}else{
			$diff = 7- abs($dayForDate-$day);
		}
		$format="D-w-Y-m-d";
		$timestamp=mktime(0,0,0,$month,$date+$diff ,$year);
		$result = explode("-",date($format,$timestamp));
		return $result;
	}
}