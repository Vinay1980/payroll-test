<?php
/*
 * Class Name: Library_Utility_SaveCSV
 * Author: VINAY KANT SAHU
 * Purpose: Utility to for operations related to CSV
 * Created Date: Sep 10, 2015
 * */
final class Library_Utility_SaveCSV{
	/*
	* Function Name: saveFile
	* Purpose: To save CSV file for given filename and Array
	* In Param: $fileName, &$array
	* Out Param: none
	* */
	public static function saveFile($fileName, &$array){
		try{
			ob_start();
			$df = fopen($fileName, 'w');
			fputcsv($df, array_keys(reset($array)));
			foreach ($array as $row) {
				fputcsv($df, $row);
			}
			fclose($df);
			ob_get_clean();
		}catch(Exception $error){
			print("Unable to save file at this time.");
		}
	}
	/*
	* Function Name: downloadFile
	* Purpose: To download CSV file for given filename and Array
	* In Param: $fileName, &$array
	* Out Param: none
	* */
	public static function downloadFile($fileName, &$array){
		try{
			$this->saveFile($fileName, $array);
			$now = gmdate("D, d M Y H:i:s");
			header("Expires: Sat, 10 Jul 2010 00:00:01 GMT"); 
			header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
			header("Last-Modified: {$now} GMT");
		
			// force download  
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
		
			// disposition / encoding on response body
			header("Content-Disposition: attachment;filename={$filename}");
			header("Content-Transfer-Encoding: binary");
		}catch(Exception $error){
			print("Unable to process file at this time.");
		}
	}
}