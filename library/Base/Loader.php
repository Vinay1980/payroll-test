<?php
/*
 * Class Name: Base_Loader
 * Author: VINAY KANT SAHU
 * Purpose: This is the base class using load all the required files and idenfication of required operations
 * Created Date: Sep 08, 2015
 * */
date_default_timezone_set ("Europe/London");
require_once ROOT_PATH .DS.'config/settings.php';
error_reporting(ShowError);
register_shutdown_function( "fatal_handler" );
/*
 * Function Name: fatal_handler
 * Purpose: To handle fatel errors 
 * In Param: $className
 * Out Param: None
 * */
function fatal_handler() {
	$error = error_get_last();
	if( $error !== NULL) {
		include_once ROOT_PATH .DS.'controllers/ErrorController.php';
		$obj = new ErrorController($this);
		$obj->indexMethod();
	}
}
/*
 * Function Name: __autoload
 * Purpose: Auto class file loader 
 * In Param: $className
 * Out Param: None
 * */
function __autoload($className) {
	try{
		$pathPart = explode("_",$className); // 
		$lastInde = sizeof($pathPart)-1;
		$fileName =  lcfirst( $pathPart[$lastInde]).".php";
		$a = array_pop($pathPart);
		$path = ROOT_PATH .DS.strtolower( implode(DS, $pathPart ) );
		include_once $path.DS.$fileName;
	}catch(Exception  $error){  
	}	
}
include_once 'Controllers.php';

final class Base_Loader{
	 public $controller;
	 public $actionName;
	 public $parameters;
	/*
	* Function Name: __construct
	* Purpose: class constructor, perform all the basic operations before actual requested task
	* In Param: none
	* Out Param: None
	* */
	 public function __construct()
     {
		 $_SERVER["REQUEST_URI"]= str_replace("//","/",$_SERVER["REQUEST_URI"]); 
		 $urlParam = explode("/",$_SERVER["REQUEST_URI"]); 
		 $orgParam = $urlParam;
		 $holder = array_shift($urlParam );
		 if(BaseDir){
		 	$holder = array_shift($urlParam ); 
		 }
		 $this->controller = (!empty($urlParam[0]))? $urlParam[0]:"index";
		 $this->actionName = (!empty($urlParam[1]))? $urlParam[1]:"index";
		 
		 $holder = array_shift($urlParam ); // to remove controller
		 $holder = array_shift($urlParam ); // to remove action
		 $this->parameters = $urlParam;
		 try{
			 include_once ROOT_PATH .DS.'controllers/'.$this->controller.'Controller.php'; // call requested controller
			 $con = $this->controller."Controller";
			 $obj = new $con($this);
			 $obj->firstRun(); // initial operations
			 $fun = $this->actionName."Method";
			 $obj->$fun(); // call requested action
			 $obj->lastRun(); // final operations
			 $obj->__destruct();
		 }catch(Exception  $error){  
		 	//print_r($error);
    		include_once ROOT_PATH .DS.'controllers/ErrorController.php';
			$obj = new ErrorController($this);
			$obj->indexMethod();
	    }
	}
}