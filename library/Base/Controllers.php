<?php
/*
 * Class Name: Base_Controllers
 * Author: VINAY KANT SAHU
 * Purpose: This is the base controller class for all the operation controller classe
 * Created Date: Sep 09, 2015
 * */
class Base_Controllers
{
	protected $loader;
	protected $postParam;
	protected $getParam;
	protected $param;
	protected $tpl;
	protected $viewDisplay = true;
	protected $viewHeader = "views/common/header.tpl";
	protected $viewContent = null;
	protected $viewFooter = "views/common/footer.tpl";
	/*
	* Function Name: __construct
	* Purpose: class constructor, perform all the basic operations before actual requested task
	* In Param: none
	* Out Param: None
	* */
	public function __construct($loaderCls)
    {
		$this->loader = $loaderCls;
		$this->postParam = $_POST;
		$this->getParam = $_GET;
		$j = 0;
		if(isset($this->loader->parameters) && sizeof($this->loader->parameters)){
			for($i = 0; $i < ceil( sizeof($this->loader->parameters)/2); $i++){// extracting parameters into key value array
				$key = $this->loader->parameters[$j];
				$j++;
				$this->param[$key] = (isset($this->loader->parameters[$j]))?$this->loader->parameters[$j]:null;
				$j++;
			}
		}
	}
	/*
	* Function Name: firstRun
	* Purpose: This could be override by child class to perform initial operations
	* In Param: none
	* Out Param: None
	* */
	public function firstRun()
    {
		//default function to run before actual action
	}
	/*
	* Function Name: setView
	* Purpose: To set display of view part
	* In Param: $value bool
	* Out Param: None
	* */
	public function setView($value = true){
		$this->viewDisplay = $value;
	}
	/*
	* Function Name: setViewVal
	* Purpose: To set variables to be used in view part
	* In Param: $key String, $value Mixed
	* Out Param: None
	* */
	public function setViewVal($key, $value = null){
		$this->tpl[$key] = $value;
	}
	/*
	* Function Name: setViewBody
	* Purpose: To set path for view body part file
	* In Param: $path (Must be under baseroot folder)
	* Out Param: None
	* */
	public function setViewContent($path){
		$this->viewContent = $path;
	}
	/*
	* Function Name: setViewHeader
	* Purpose: To set path for view header part file
	* In Param: $path (Must be under baseroot folder)
	* Out Param: None
	* */
	public function setViewHeader($path){
		$this->viewHeader = $path;
	}
	/*
	* Function Name: setViewFooter
	* Purpose: To set path for view Footer part file
	* In Param: $path (Must be under baseroot folder)
	* Out Param: None
	* */
	public function setViewFooter($path){
		$this->viewFooter = $path;
	}
	/*
	* Function Name: getParam
	* Purpose: To get variables passed to action by url/post/get method 
	* In Param: $key String, $type String (post/get/param)
	* Out Param: $value Mixed
	* */
	public function getParam($key, $type=null){
		$value = null;
		switch(strtolower($type)){
			case "post":
				$value = (isset($this->postParam[$key]))?$this->postParam[$key]:null;
			break;
			case "get":
				$value = (isset($this->getParam[$key]))?$this->getParam[$key]:null;
			break;
			default:
				$value = (isset($this->param[$key]))?$this->param[$key]:null;
			break;
			}
		return $value;	
	}
	/*
	* Function Name: redirectTo
	* Purpose: To redirect within framework 
	* In Param: $controller,$method
	* Out Param: None
	* */
	public function redirectTo($controller,$method){
		header("Location:".BaseURL."/$controller/$method");
		die;
	}
	/*
	* Function Name: lastRun
	* Purpose: This could be override by child class to perform final operations
	* In Param: None
	* Out Param: None
	* */
	public function lastRun(){
		//default function to run at the end of actual action	
		$controller = $this->loader->controller;
		$method = $this->loader->actionName;
		if(empty($this->viewContent)) $this->viewContent = "views/template/$controller/$method.tpl";
		if($this->viewDisplay){
			include_once(ROOT_PATH .DS.$this->viewHeader); //headr file
			include_once(ROOT_PATH .DS.$this->viewContent); //headr file
			include_once(ROOT_PATH .DS.$this->viewFooter); //footer file
		}
	} 
	/*
	 * Function Name: __destruct
	 * Purpose:  Destructor  
	 * In Param: None
	 * Out Param: None
	 * */
    function __destruct(){
    }
}